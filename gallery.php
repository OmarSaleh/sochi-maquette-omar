<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Gallery | Sochi</title>
    <?php include("../Sochi/resources.php") ?>
</head>
    <body class="contact-bg">
        <?php
        include("../Sochi/darkheader.html");
        ?>
        <main>
            <div class="content-news">
                <h1>Gallery.</h1>
                <p>Sochi has a lot to offer for anyone who loves nature, sports, history,        
                <br/> sunny beach leisure and active adventures.</p>
                <ul class="news__links" id="myDIV">
                    <li><a class="clicky active" href="#">All</a></li>
                    <li><a class="clicky" href="#">Rooms</a></li>
                    <li><a class="clicky" href="#">City</a></li>
                    <li><a class="clicky" href="#">Beaches</a></li>
                    <li><a class="clicky" href="#">Mountain Resort</a></li>
                </ul>
            </div>
            <div class="container-gallery">
            
            <div class="row"> 
                <div class="column">
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                </div>
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Park Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                </div>  
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>Beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                </div>
            </div>
            <div class="row"> 
                <div class="column">
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                </div>
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Park Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                </div>  
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>Beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                </div>
            </div>
            <div class="row"> 
                <div class="column">
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                </div>
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Park Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                </div>  
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>Beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                </div>
            </div>
            <div class="row"> 
                <div class="column">
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                   <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                </div>
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Park Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>discovery</p> Discovery Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                </div>  
                <div class="column">
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachblue.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/twinbuildings.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Tourism</p> Tourism Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/beachbg.png" style="width:100%"><div class="hover-gallery-text"><h3><p>Beaches</p> Beaches Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/woods.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/treepath.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/skying.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Skying</p> Skying Sochi</h3></div></div> 
                    <div class="hover-gallery"><img class="gallery-img" src="../Sochi/assets/gallery/naturecascade.jpg" style="width:100%"><div class="hover-gallery-text"><h3><p>Nature</p> Nature Sochi</h3></div></div> 
                </div>
            </div>
           
                <a href="#" id="loadMore">Load More</a>
            </div>
        </main>
        <?php
        include("../Sochi/darkfooter.html");
        ?>
        <main>
    </body>
    
</html>

