$(document).ready(function() {
    $(".box-news").slice(0, 6).show();
    $("#loadMore").on("click", function(e) {
        e.preventDefault();
        $(".box-news:hidden").slice(0, 6).slideDown();
        if ($(".box-news:hidden").length == 0) {
            $("#loadMore").text("End of Gallery ;)").addClass("noContent");
        }
    });

})