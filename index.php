<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include("../Sochi/resources.php"); ?>
    <title>Homepage  | Sochi</title>
    
</head>

<body>
<?php
    include("../Sochi/lightheader.html");
?>
    <main>
        <div class="homepage-container">
            <div class="bg-homepage-img"><img src="../Sochi/assets/homepage/homepagebgimg.jpeg" alt=""></div>
            <div class="homepage-header">
                <p>welcome to sochi hotel</p>
                <h1>Feeling good like <br/> your favorite place.</h1>
                <div class="video-box" id="video-box-id">
                <span>Adventure</span>
                    <a href="../Sochi/assets/post/videobeach.mp4" class="video_trigger">
                        <div class="play-btn">
                            <i class="fas fa-play"></i>
                        </div>
                    </a>
                </div>
            </div>
            <div class="booking-box">
                <div class="check-in-out">
                    <ul>
                        <li>
                            <span>check in</span>
                            <a href="#"><p>Mon, 19 Aug 2019<i class="fas fa-caret-down"></i></p></a>
                            
                        </li>
                        <li>
                            <span>check out</span>
                            <a href="#"><p>Sat, 24 Aug 2019<i class="fas fa-caret-down"></i></p></a>
                            
                        </li>
                        <li>
                            <span>person</span>
                            <a href="#"><p>1 Adult<i class="fas fa-caret-down"></i></p></a>
                            
                        </li>
                        <li class="book-now-btn">
                            <div class="orng-book">
                                <span>next</span>
                                <a href="#"><i class="fas fa-minus"></i></a>
                                <p>book now</p>
                            </div>
                        </li>
                    </ul>
                </div>                
            </div>
            <div class="about-homepage" id="title-homepage-container">           
                    <span>About us</span>
                    <h1>Begin your amazing adventure.</h1>             
            </div>
            <div class='some-page-wrapper'>
                <div class='row-hp'>
                    <div class='column-hp'>
                        <div class='orange-column'>
                        The humid subtropical climate, high mountains, exotic vegetation, endless beaches, national parks, historic architecture, exciting attraction sites, art festivals and lively multicultural environment make Sochi a prominent resort destination. 
                        </div>
                    </div>
                    <div class='column-hp'>
                        <div class='blue-column'>
                        Sochi has a lot to offer for anyone who loves nature, sports, history, sunny beach leisure and active adventures. There is too much to do and too many things to see in Sochi so you will never be bored.
                        </div>
                    </div>
                </div>
                <div class='row-hp'>
                    <div class='column-hp'>
                        <div class='green-column'>
                            <img src="../Sochi/assets/homepage/beachprettytotaly.jpg" alt="">
                            <div class="bottom-right-more"><a href="#"><p>Explore More <i class="fas fa-long-arrow-alt-right"></i> </p></a></div>
                        </div>
                    </div>
                    <div class='column-hp'>
                        <div class='orange-column'>
                            <img src="../Sochi/assets/homepage/chinaissobeautifuliloveitsomuch.jpg" alt="">
                        </div>
                    </div>                    
                </div>
            </div>
            <div class="about-homepage" id="title-homepage-container">           
                    <span>rooms</span>
                    <h1>Rooms / Suites.</h1>             
            </div>
            <div class="lt-grid-container">
                <div class="lt-big-pic bottom-left-box">
                    <img class="rooms__img" src="../Sochi/assets/rooms/superniceroom.jpg" alt="image 1">
                    <div class="popular-index"><p>Popular</p></div>
                    <a href="#">
                        <div class="big-rooms-box-title-index">
                            <h3>Comfort Room</h3>
                            <div class="price-tag-big-box-index">
                                <p>$89</p>
                                <span> night</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lt-bottom-left bottom-left-box">
                    <img class="rooms__img" src="../Sochi/assets/rooms/luxroom.jpg" alt="image 2">
                  
                    <a href="#">
                        <div class="big-rooms-box-title-index">
                            <h3>Luxe Room</h3>
                            <div class="price-tag-big-box-index">
                                <p>$259</p>
                                <span> night</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lt-bottom-center bottom-left-box">
                    <img class="rooms__img" src="../Sochi/assets/rooms/grandomegadeluxroom.jpg" alt="image 3">
                   
                    <a href="#">
                        <div class="big-rooms-box-title-index">
                            <h3>Grand Delux Room</h3>
                            <div class="price-tag-big-box-index">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lt-bottom-right bottom-left-box">
                    <img class="rooms__img" src="../Sochi/assets/rooms/standardroom.jpg" alt="image 4">
                   
                    <a href="#">
                        <div class="big-rooms-box-title-index">
                            <h3>Standard Room</h3>
                            <div class="price-tag-big-box-index">
                                <p>$49</p>
                                <span> night</span>
                            </div>
                        </div>
                    </a>
                </div>
                <div class="lt-top-right bottom-left-box">
                    <img class="rooms__img" src="../Sochi/assets/rooms/apartment.jpg" alt="image 5">
                    
                    <a href="#">
                        <div class="big-rooms-box-title-index">
                            <h3>Apartments</h3>
                            <div class="price-tag-big-box-index">
                                <p>$649</p>
                                <span> night</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="middle-text-homepage">
                <p>We strive to be the best in our field to make you even more comfortable.</p>
            </div>
            <div class="stats-and-numbers">
                <div class="list-stats">
                    <ul>
                        <li>
                            <span>spa offers</span>
                            <p>32+</p>
                        </li>
                        <li>
                            <span>rooms</span>
                            <p>24+</p>
                        </li>
                        <li>
                            <span>beaches</span>
                            <p>3+</p>
                        </li>
                        <li>
                            <span>happy clients</span>
                            <p>+10k</p>
                        </li>
                    </ul>
                </div>                
            </div>
            <div class="room-adventure">
                <div class="room-adventure-box">
                    <div class="left-part-adv">
                        <h2>Make room for adventure.</h2>
                        <p>Book your room right now and start your amazing adventure full of discoveries and experiences with Sochi.</p>
                    </div>
                    <div class="right-part-adv">
                        <a class="white-reserv" href="#">
                            <button class="white-res-btn">Reservations
                                <i class="fas fa-long-arrow-alt-right"></i>
                            </button>
                        </a>     
                    </div>
                </div>  
            </div>
        </div>
    </main>
    <?php
    include("../Sochi/darkfooter.html");
    ?>
</body>
</html>