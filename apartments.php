<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Apartments | Sochi</title>
    <?php include("../Sochi/resources.php"); ?>
</head>

<body>

    <!-- Header => Light header -->
    <?php include("../Sochi/lightheader.html"); ?>
    <!-- /Header => Light header -->

    <!-- Main Content -->
        <main>
            <div class="main-bg-apts">
                <img class="apartments-bg" src="../Sochi/assets/apartments/beautapart.jpg" alt="beautyapart">
                <div class="white-box-apts"></div>
            </div>
            <div class="apts-grid-container">
                <div class="apts-Apartments">
                    <p>Rosa Kuthor</p>
                    <h1>Apartments</h1>
                </div>
                <div class="apts-sq-ft">
                    <img src="../Sochi/assets/apartments/icon-square.svg" alt="square">
                    <p>1500 sq.ft.</p>
                </div>
                <div class="apts-bedrooms">
                    <img src="../Sochi/assets/apartments/icon-bed.svg" alt="bed">
                    <p>4 Bedrooms</p>
                </div>
                <div class="apts-bathroom">
                    <img src="../Sochi/assets/apartments/icon-bath.svg" alt="bath">
                    <p>2 Bathrooms</p>
                </div>
                <div class="apts-description">
                    <div class="apts-description-title">
                        <h2>Description</h2>
                    </div>
                    <div class="apts-description-content">
                        <p>Modern open plan super spacious penthouses in 
                            historic Sochi . Windows from floor to ceiling 
                            to give you the best views of Sochi and full view 
                            of majestic Table Mountain anywhere in the penthouse.
                            The big patio with jacuzzi bath is ideal to watch the 
                            sunset and the city lights. Coffee shops, museums, 
                            theatre within walking distance. Separate scullery/laundry 
                            & full open plan kitchen & shared pool.24 hour security
                            and indoor parking included.
                        </p>
                        <h2>Amenity</h2>
                    </div>
                    
                </div>
                <div class="apts-amenity">
                    
                    <div class="free-parking">
                        <img src="../Sochi/assets/apartments/shape.svg" alt="parking">
                        <p>Free parking</p>
                    </div>
                    <div class="elevator">
                        <img src="../Sochi/assets/apartments/icon-elevator.svg" alt="elevator">
                        <p>Elevator</p>
                    </div>
                    <div class="coffee-maker">
                        <img src="../Sochi/assets/apartments/icon-coffee-maker.svg" alt="coffee">
                        <p>Coffee maker</p>
                    </div>
                    <div class="fast-wifi">
                        <img src="../Sochi/assets/apartments/icon-wifi.svg" alt="wifi">
                        <p>Fast Wi-fi</p>
                    </div>
                    <div class="cable-tv">
                        <img src="../Sochi/assets/apartments/icon-tv.svg" alt="cable">
                        <p>Cable TV</p>
                    </div>
                    <div class="conditioning">
                        <img src="../Sochi/assets/apartments/icon-conditioner.svg" alt="condi">
                        <p>Conditioning</p>
                    </div>
                    <div class="spa-services">
                        <img src="../Sochi/assets/apartments/icon-spa.svg" alt="spa">
                        <p>SPA Services</p>
                    </div>
                    <div class="kitchen-room">
                        <img src="../Sochi/assets/apartments/icon-kitchen.svg" alt="kitchen">
                        <p>Kitchen</p>
                    </div>
                    <div class="hair-dryer">
                        <img src="../Sochi/assets/apartments/icon-hair-dryer.svg" alt="hair">
                        <p>Hair Dryer</p>
                    </div>
                    <div class="gym-room">
                        <img src="../Sochi/assets/apartments/icon-sport.svg" alt="gym">
                        <p>Gym</p>
                    </div>
                    <div class="pool-room">
                        <img src="../Sochi/assets/apartments/icon-pool.svg" alt="pool">
                        <p>Pool</p>
                    </div>
                    <div class="washer-room">
                        <img src="../Sochi/assets/apartments/icon-washer.svg" alt="washer">
                        <p>Washer</p>
                    </div>
                </div>
                <div class="apts-apartment-pics">
                    
                    <img src="../Sochi/assets/rooms/apartment.jpg" alt="apartment">
                    <div class="left-right-chev">
                       <button class="left-chev-font">  
                           <i class="fas fa-chevron-right"></i>
                        </button>
                        <hr class="horizontal"/>
                        <button class="right-chev-font">
                            <i class="fas fa-chevron-left"></i>
                        </button>
                    </div>
                </div>
                <div class="apts-reviews">         
                    <div class="reviews-title">
                        <h2>24 Reviews</h2>
                    </div>
                    <div class="apts-reviews-grid">
                        <div class="comfort-reviews">
                            <p>Comfort</p><span>7.5</span>
                            <div class="container-apts-reviews">
                                <div class="skills comfort"></div>
                            </div>
                        </div>
                        <div class="food-reviews">
                            <p>Food</p><span>8.6</span>
                            <div class="container-apts-reviews">
                                <div class="skills food"></div>
                            </div>
                        </div>
                        <div class="facilities-reviews">
                            <p>Facilities</p><span>9.2</span>
                            <div class="container-apts-reviews">
                                <div class="skills facili"></div>
                            </div>
                        </div>
                        <div class="location-reviews">
                            <p>Location</p><span>9.0</span>
                            <div class="container-apts-reviews">
                                <div class="skills location"></div>
                            </div>
                        </div>
                        <div class="staff-reviews">
                            <p>Staff</p><span>9.2</span>
                            <div class="container-apts-reviews">
                                <div class="skills staff"></div>
                            </div>
                        </div>
                        <div class="price-qual-reviews">
                            <p>Price-quality ratio</p><span>7.6</span>
                            <div class="container-apts-reviews">
                                <div class="skills price-qual"></div>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="apts-review-one">
                    <div class="review-one-container">
                        <h4>Best hotel !</h4>
                        <p>The hotel has everything you need. On the ground floor there is a lobby bar, on the second floor there is a zone with an indoor pool and sauna, on the seventh floor there is a restaurant and spa-salon. The rooms are cleaned every day.</p>
                        <div class="reviews-contact-bot">
                            <i class="far fa-user"></i>
                            <div class="reviews-author">
                                <p>Jacob Lane</p>
                                <span>from USA</span>      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="apts-review-two">
                    <div class="review-one-container">
                        <h4>Comfortable hotel.</h4>
                        <p>Well, what can I say, every year, day and hour, this place is being transformed for the better. The staff is completely competent and friendly, Everything around is blooming, pleasing, nourishing and making the holiday bright.</p>
                        <div class="reviews-contact-bot">
                            <i class="far fa-user"></i>
                            <div class="reviews-author">
                                <p>Victoria Wilson</p>
                                <span>from France</span>      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="apts-review-three">
                    <div class="review-one-container">
                        <h4>The modern.</h4>
                        <p>The modern 5 * Hotel Sochi Center is an ideal solution for combining business and leisure. Stylish design and exceptional service will satisfy the desires of any guest. 150 rooms with balcony (non-smoking), sea view, trendy restaurant.</p>
                        <div class="reviews-contact-bot">
                            <i class="far fa-user"></i>
                            <div class="reviews-author">
                                <p>Max Edwards</p>
                                <span>from Germany</span>      
                            </div>
                        </div>
                    </div>
                </div>
                <div class="apts-bottom-nav-bar-page">
                    <a href="#"><i class="fas fa-arrow-left"></i></a>
                    <ul>
                        <li><a href="">1</a></li>
                        <li><a href="">2</a></li>
                        <li><a href="">3</a></li>
                        <li><a href="">4</a></li>
                        <li><a href="">...</a></li>
                        <li><a href="">10</a></li>
                    </ul>
                    <a class="right-arrow-final" href="#"><i class="fas fa-arrow-right"></i></a>
                </div>
                <div class="apts-price">
                    <ul>
                        <li><p>price</p></li>
                        <li class="price-apts-tag"><h4>$129</h4><span>night</span></li>
                    </ul>
                </div>
                <div class="apts-reservation">
                    <div class="dates-calendar-apts">

                    </div>
                    <a class="res-apts" href="#"><button class="orange-btn-apts">Reservations</button></a>
                    <p>Until you pay for anything</p>
                </div>
                <div class="apts-map">
                    <iframe class="googlemaps-apts" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2745.621024245574!2d6.6246710198281225!3d46.51561162109041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c319a0d293ee9%3A0x7df48d393883c1d2!2sSQLI%20SUISSE%20SA!5e0!3m2!1sfr!2sch!4v1590024920123!5m2!1sfr!2sch" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
                <div>
            </div>
        </main>
    <!-- /Main Content -->

    <!-- Footer => Light footer -->
    <?php include("../Sochi/darkfooter.html"); ?>
    <!-- /Footer => Light footer -->

</body>

</html>