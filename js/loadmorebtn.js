$(document).ready(function() {
    $(".column").slice(0, 3).show();
    $("#loadMore").on("click", function(e) {
        e.preventDefault();
        $(".column:hidden").slice(0, 3).slideDown();
        if ($(".column:hidden").length == 0) {
            $("#loadMore").text("End of Gallery ;)").addClass("noContent");
        }
    });

})