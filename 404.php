<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <title>Error | Page 404  | Sochi</title>
    <?php include("../Sochi/resources.php"); ?>
</head>

<body class="error-bg">

    <!-- Header => Light header -->
    <?php include("../Sochi/lightheader.html"); ?>
    <!-- /Header => Light header -->

    <!-- Main Content -->
        <main>
            <div class="container-404">
                <img src="../Sochi/assets/404/404.png">
                <p>this page is a myth</p>
                <a class="backtohome" href="../Sochi/index.php">
                    <button class="homepage-btn">
                        Back to Homepage
                    </button>
                </a>
            </div>
        </main>
    <!-- /Main Content -->

    <!-- Footer => Light footer -->
    <?php include("../Sochi/lightfooter.html"); ?>
    <!-- /Footer => Light footer -->

</body>

</html>