<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <?php include("../Sochi/resources.php"); ?>
    <title>Rooms | Sochi</title>
</head>

<body>
<?php
    include("../Sochi/darkheader.html");
?>
    <main>
        <div class="title-rooms-page">
            <h1>Rooms/Suites.</h1>
        </div>
        <div class="container-rooms">
            <div class="rooms-suits-top">
                
                <div class="big-img-rooms">
                    <img src="../Sochi/assets/rooms/superniceroom.jpg" alt="deluxroompic">
                    <div class="popular"><p>Popular</p></div>
                    <a href="#">
                        <div class="big-rooms-box-title">
                            <h3>Grand Delux Room</h3>
                            <div class="price-tag-big-box">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </div>
                    </a>
                </div>
            </div>
            <div class="container-rooms-flex"> 
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/superniceroom.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Grand Delux Room</h3>
                            <div class="price-tag">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Adler 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">925 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">2 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex"><div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/luxroom.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Luxe Room</h3>
                            <div class="price-tag">
                                <p>$259</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Sochi 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">980 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">3 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/apartment.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Apartments</h3>
                            <div class="price-tag">
                                <p>$189</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Rosa Khutar
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">1500 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">4 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/standardroom.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Standard Room</h3>
                            <div class="price-tag">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Adler 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">925 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">2 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/grandomegadeluxroom.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Comfort Room</h3>
                            <div class="price-tag">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Adler 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">925 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">2 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/standardroom.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Standard Room</h3>
                            <div class="price-tag">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Adler 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">925 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">2 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/superniceroom.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Comfort Room</h3>
                            <div class="price-tag">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Adler 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">925 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">2 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/luxroom.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Grand Delux Room</h3>
                            <div class="price-tag">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Adler 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">925 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">2 Bedrooms
                        </div>
                    </div>
                </div>
                <div class="box-rooms-flex">
                    <div class="img-wrapper">
                        <img src="../Sochi/assets/rooms/apartment.jpg" alt="deluxroom">
                        <a href="#">
                            <h3>Grand Delux Room</h3>
                            <div class="price-tag">
                                <p>$129</p>
                                <span> night</span>
                            </div>
                        </a>
                    </div>
                    <div class="stats-rooms">
                        <img class="map-icon" src="../Sochi/assets/rooms/icon-location.svg">Adler 
                        <div class="bottom-stats-rooms">
                            <img class="square-icon" src="../Sochi/assets/rooms/icon-square.svg">925 sq.ft.
                            <img class="bed-icon" src="../Sochi/assets/rooms/icon-bed.svg">2 Bedrooms
                        </div>
                    </div>
                </div>
                
            </div>
        </div>
    </main>
    <?php
    include("../Sochi/darkfooter.html");
?>
</body>
</html>