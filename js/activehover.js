document.addEventListener("DOMContentLoaded", function(e) {
    var header = document.getElementById("myDIV");
    var btns = document.getElementsByClassName("clicky");
    for (var i = 0; i < btns.length; i++) {
        btns[i].addEventListener("click", function(event) {
            var current = document.getElementsByClassName("active").item(0);
            current.classList.remove("active");
            event.target.classList.add("active");
        });
    }
});