<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include("../Sochi/resources.php"); ?>
    <title>Contact Us  | Sochi</title>
</head>
    <body class="contact-bg">
        <?php
        include("../Sochi/darkheader.html");
        ?>
        <main>
            <div class="content-contact">
                <h1>Let's get in touch.</h1>
                <ul>
                    <li><h2>Address</h2><p>23400 S Western Ave,<br/> Harbor City, CA 90710</p></li>
                    <li class="contact-class"><h2>Contact</h2><p>hello@example.com<br/> +1 514.123.4567</p></li>
                    <li><h2>Follow Us</h2><p>Connect with me on <u>facebook</u>,<br/> <u>twitter</u> or <u>instagram</u></p></li>
                </ul>
            </div>
            <iframe class="googlemaps" src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d2745.621024245574!2d6.6246710198281225!3d46.51561162109041!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x478c319a0d293ee9%3A0x7df48d393883c1d2!2sSQLI%20SUISSE%20SA!5e0!3m2!1sfr!2sch!4v1590024920123!5m2!1sfr!2sch" frameborder="0" style="border:0;" allowfullscreen="" aria-hidden="false" tabindex="0"></iframe>
        </main>
        <?php
        include("../Sochi/darkfooter.html");
        ?>
    </body>
</html>