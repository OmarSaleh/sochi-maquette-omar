<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <?php include("../Sochi/resources.php"); ?>
    <title>Post | Sochi</title>
</head>
<body>
<?php
    include("../Sochi/darkheader.html");
?>
    <main>
    <div class="container-post">
        <div class="title-post" id="top-bottom-post">
            <span>Adventure - 10 Sep 2019</span>
            <h1>Hurry up to get a ticket for a Mediterranean trip on a cruise ship.</h1>
        </div>
        <div class="small-line-post"></div>
        <img class="img-cruise" src="../Sochi/assets/post/cruise.jpg">
        <div class="sidebar-social">
                <ul class="list-sidebar-social">
                    <li>
                        <a href="#" ><h4>Share</h4></a>
                    </li>
                    <li>
                        <div class="sharing-line"></div>
                    </li>
                    <li>
                        <a href="#" ><i class="fab fa-facebook-square"></i></a>
                    </li>
                    <li>
                        <a href="#" ><i class="fab fa-twitter"></i></a>
                    </li>
                    <li>
                        <a href="#"><i class="fab fa-linkedin-in"></i></a>
                    </li>
                </ul>
            </div>
        <div class="content-post">
          
            <p class="bold-text-part">There’s the whole Buddhist thing about the essence of a bowl being its emptiness—that’s why it’s useful.
                Its emptiness allows it to hold something. I guess that means that design must talk about something else.
                If you make design about design, you’re just stacking bowls, and that’s not what bowls are for. 
            </p>
            <br/>
            <p>
                A designer is an emerging synthesis of artist, inventor, mechanic, objective economist, and evolutionary strategist. <br/>

                Above all, think of life as a prototype. We can conduct experiments, make discoveries, and 
                change our perspectives. We can look for opportunities to turn processes into projects that have tangible outcomes.
                We can learn how to take joy in the things we create whether they take the form of a fleeting experience or an heirloom
                that will last for generations. We can learn that reward comes in creation and re-creation, no just in the consumption
                of the world around us. Active participation in the process of creation is our right and our privilege. We can learn to
                measure the success of our ideas not by our bank accounts by their impact on the world. <br/>

                It is not enough that we build products that function, that are understandable and usable, we also need to build products
                that bring joy and excitement, pleasure and fun, and, yes, beauty to people’s lives. Creativity is to discover a question 
                that has never been asked. If one brings up an idiosyncratic question, the answer he gives will necessarily be unique as well.
            </p>
            <div class="video-box" id="video-box-id">
                <a href="../Sochi/assets/post/videobeach.mp4" class="video_trigger">
                    <video><source src="../Sochi/assets/post/videobeach.mp4" type="video/mp4"></video>
                    <div class="play-btn">
                        <i class="fas fa-play"></i>
                    </div>
                </a>
            </div>
            <h3>How well we communicate.</h3>
            <p>Here is one of the few effective keys to the design problem: the ability of the designer to recognize as
                many of the constraints as possible; his willingness and enthusiasm for working within these constraints.
                The most profound technologies are those that disappear. They weave themselves into the fabric of everyday
                life until they are indistinguishable from it.<br/>

                Above all, think of life as a prototype. We can conduct experiments, make discoveries, and change our
                perspectives. We can look for opportunities to turn processes into projects that have tangible outcomes.
                We can learn how to take joy in the things we create whether they take the form of a fleeting experience
                or an heirloom that will last for generations. We can learn that reward comes in creation and re-creation,
                no just in the consumption of the world around us. <br/>

                Active participation in the process of creation is our right and our privilege. 
                We can learn to measure the success of our ideas not by our bank accounts by their impact on the world.
            </p>
            <div class="page-quote">
                <p>The goal of a designer is to listen, observe, understand, sympathize, empathize,
                 synthesize, and glean insights that enable him or her to make the invisible visible.
                </p><span>— Francisco Hawkins</span>
            </div>
            <p>There’s the whole Buddhist thing about the essence of a bowl being its emptiness—that’s why it’s useful.
                Its emptiness allows it to hold something. I guess that means that design must talk about something else.
                If you make design about design, you’re just stacking bowls, and that’s not what bowls are for. <br/>

                A designer is an emerging synthesis of artist, inventor, mechanic, objective economist, and evolutionary strategist.</p>
        </div>
        <div class="small-line-post"></div>
        <div class="next-post" id="top-bottom-post">
            <span>Next Post</span>
            <h3>New luxury cozy houses for all families at attractive prices.</h3>
            <button class="nxt-orange-btn"><i class="fa fa-arrow-right"></i></button>
        </div>    
    </div>
    </main>
    <?php
    include("../Sochi/darkfooter.html");
    ?>
</body>
</html>